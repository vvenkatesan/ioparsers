#include "IOParsers.h"

typedef struct {
  double max_wtime;
  double avg_wtime;
  double min_wtime;
  double max_ctime;
  double avg_ctime;
  double min_ctime;
  double max_exch;
  double avg_exch;
  double min_exch;
  double write_bandwidth;
}tile_tuple;


class TileIOParser: public IOParsers{

 public:
  TileIOParser(int argc, char *argv[]); 
  int io_parser(ifstream &ip);
  void initialize_tuple (tile_tuple &t);
};



