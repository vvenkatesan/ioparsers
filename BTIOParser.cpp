#include "BTIOParser.h"

BTIOParser :: BTIOParser(int argc, char *argv[]) :
  IOParsers (argc, argv) {
  
  ifstream ip;
  open_file_read(ip);
  io_parser(ip);

}



int BTIOParser :: io_parser(ifstream &ip){
  
  bt_tuple t;
  int count = 0, x = 0;
  string orig_tname = testname;
  string query, splits;
  initialize_tuple(t);
  open_database();
  create_query_object();
  query = string("create TABLE if not exists BTIO (Testname varchar(30), Experiment varchar(30), nprocs varchar(20), csb varchar(20), aggregators varchar(30), MaxWtime REAL, AvgWtime REAL, MinWtime REAL, MaxWCtime REAL, AvgWCtime REAL, MinWCtime REAL,MaxWExch REAL, AvgWExch REAL, MinWExch REAL, MaxRtime REAL, AvgRtime REAL, MinRtime REAL, MaxRCtime REAL, AvgRCtime REAL, MinRCtime REAL, MaxRExch REAL, AvgRExch REAL, MinRExch REAL, TotalTime REAL, App_time REAL, IOPercent REAL, DataSize REAL, Bandwidth REAL, Fcoll varchar(30), Mapping varchar(30), btioclass varchar(20),PRIMARY KEY(Testname))");
  execute_query(query);
  testname = orig_tname + tostring(x+1);
  while (x < num_runs){
    //Lets Skip till the first line
    int list_cnt;
    list<string> vals;
    if (count == -1){
      testname = orig_tname + tostring(x+1);
      count++;
    }
    if (count == 0){
      string temp;
      char temp_extract[256];
      memset(&temp_extract[0], 0, sizeof(temp_extract));
      vals.erase(vals.begin(),vals.end());
      temp = getLine("MAX-WRITE", ip);
      ip.getline(temp_extract, 256);
      temp = string(temp_extract);
      trim(temp);
      splits=" ";
      vals = split_tokens(temp, splits);
      list_cnt = vals.size();
      for (int i=0; i<list_cnt; i++){
	switch (i){
	case 0:
	  t.max_wtime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 1:
	  t.avg_wtime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 2:
	  t.min_wtime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 3:
	  t.max_wctime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 4:
	  t.avg_wctime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 5:
	  t.min_wctime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 6:
	  t.max_wexch = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 7:
	  t.avg_wexch = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 8:
	  t.min_wexch = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	}
      }
      count++;
    }
    if (count == 1){
      string temp;
      char temp_extract[256];
      memset(&temp_extract[0], 0, sizeof(temp_extract));
      vals.erase(vals.begin(),vals.end());
      temp = getLine("MAX-READ", ip);
      ip.getline(temp_extract, 256);
      temp = string(temp_extract);
      trim(temp);
      splits=" ";
      vals = split_tokens(temp, splits);
      list_cnt = vals.size();
      for (int i=0; i<list_cnt; i++){
	switch (i){
	case 0:
	  t.max_rtime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 1:
	  t.avg_rtime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 2:
	  t.min_rtime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 3:
	  t.max_rctime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 4:
	  t.avg_rctime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 5:
	  t.min_rctime = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 6:
	  t.max_rexch = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 7:
	  t.avg_rexch = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	case 8:
	  t.min_rexch = atof(vals.front().c_str());
	  vals.pop_front();
	  break;
	}
      }

      count++;
    }
    if (count == 2){
      string temp;
      char temp_extract[256];
      memset(&temp_extract[0], 0, sizeof(temp_extract));
      vals.erase(vals.begin(),vals.end());
      temp = getLine("BTIO -- statistics:", ip);
      ip.getline (temp_extract, 256);
      temp = string (temp_extract, 256);
      trim(temp);
      splits="I/O timing in seconds   :           ";
      vals = split_tokens(temp, splits);
      vals.pop_front();
      t.ttime = atof(vals.front().c_str());
      vals.pop_front();
      count++;
    }
    if (count == 3){
      string temp;
      char temp_extract[256];
      memset(&temp_extract[0], 0, sizeof(temp_extract));
      vals.erase(vals.begin(),vals.end());
      ip.getline(temp_extract, 256);
      temp = string(temp_extract, 256);
      trim(temp);
      splits="I/O timing percentage   :           ";
      vals = split_tokens(temp, splits);
      vals.pop_front();
      t.io_percent = atof(vals.front().c_str());
      vals.pop_front();
      count++;
    }
    if (count == 4){
      string temp;
      char temp_extract[256];
      memset(&temp_extract[0], 0, sizeof(temp_extract));
      vals.erase(vals.begin(),vals.end());
      ip.getline(temp_extract, 256);
      temp = string(temp_extract, 256);
      trim(temp);
      splits="Total data written (MB) :           ";
      vals = split_tokens(temp, splits);
      vals.pop_front();
      t.data_size = atof(vals.front().c_str());
      vals.pop_front();
      count++;
    }
    if (count == 5) {
      string temp;
      char temp_extract[256];
      memset(&temp_extract[0], 0, sizeof(temp_extract));
      vals.erase(vals.begin(),vals.end());
      ip.getline (temp_extract, 256);
      temp = string (temp_extract, 256);
      trim(temp);
      splits=" I/O data rate  (MB/sec) :         ";
      vals = split_tokens(temp, splits);
      vals.pop_front();
      t.bandwidth= atof(vals.front().c_str());
      vals.pop_front();
      count++;

    }
    if (count == 6){
      string temp;
      char temp_extract[256];
      memset(&temp_extract[0], 0, sizeof(temp_extract));
      vals.erase(vals.begin(),vals.end());
      temp = getLine("Iterations", ip);
      ip.getline (temp_extract, 256);
      temp = string (temp_extract, 256);
      trim(temp);
      splits="Time in seconds =               ";
      vals = split_tokens(temp, splits);
      vals.pop_front();
      t.apptime = atof(vals.front().c_str());
      vals.pop_front();
      execute_query(construct_query(t, testname, orig_tname));
      count=-1;
      x++;
    }



  }
  return IOPARSE_SUCCESS;
}

string BTIOParser :: construct_query (bt_tuple &t,
				      string tname, 
				      string otname){

  string query;
  string quote = "\"";
  query = string("insert into BTIO values (" +
		 quote + tname + quote + "," + 
		 quote + otname + quote + "," +
		 tostring(nprocs)+ "," +
		 tostring(csb) + "," +
		 tostring(aggs) + "," +
		 tostring(t.max_wtime) + "," +
		 tostring(t.avg_wtime) + "," +
		 tostring(t.min_wtime) + "," +
		 tostring(t.max_wctime) + "," +
		 tostring(t.avg_wctime) + "," +
		 tostring(t.min_wctime) + "," +
		 tostring(t.max_wexch) +  "," +
		 tostring(t.avg_wexch) + "," +
		 tostring(t.min_wexch) + "," +
		 tostring(t.max_rtime) + "," +
		 tostring(t.avg_rtime) + "," +
		 tostring(t.min_rtime) + "," +
		 tostring(t.max_rctime) + "," +
		 tostring(t.avg_rctime) + "," +
		 tostring(t.min_rctime) + "," +
		 tostring(t.max_rexch) +  "," +
		 tostring(t.avg_rexch) + "," +
		 tostring(t.min_rexch) + "," +
		 tostring(t.ttime) + "," +
		 tostring(t.apptime) + "," +
		 tostring(t.io_percent) + "," +
		 tostring(t.data_size) + "," +
		 tostring(t.bandwidth) + "," +
		 quote + fcoll_module + quote + "," +
		 quote + mapping + quote + "," +
		 quote + tile_size + quote +  ");");
  
  cout << query <<endl;
  return query;
}


void BTIOParser  ::  initialize_tuple (bt_tuple &t){
  
  t.max_wtime = 0.0;
  t.avg_wtime = 0.0;
  t.min_wtime = 0.0;
  t.max_wctime = 0.0;
  t.avg_wctime = 0.0;
  t.min_wctime = 0.0;
  t.max_wexch = 0.0;
  t.avg_wexch = 0.0;
  t.min_wexch = 0.0;
  t.max_rtime = 0.0;
  t.avg_rtime = 0.0;
  t.min_rtime = 0.0;
  t.max_rctime = 0.0;
  t.avg_rctime = 0.0;
  t.min_rctime = 0.0;
  t.max_rexch = 0.0;
  t.avg_rexch = 0.0;
  t.min_rexch = 0.0;
  t.ttime = 0.0;
  t.apptime = 0.0;
  t.io_percent = 0.0;
  t.data_size = 0.0;
  t.bandwidth = 0.0;

}

