/*************************************************************************************

IOParser : This application provides an uniform setting for parsing the most widely 
used IO benchmarks and applications. The basic desing also allows ways for easy extension by adding support for newer benchmarks.

University of Houston, Copyright 2012.

Vishwanath Venkatesan (venkates@cs.uh.edu)

*****************************************************************************************/
#ifndef IOPARSERS_H
#define IOPARSERS_H


#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <list>
#include <map>
#include <boost/algorithm/string.hpp>

#include "libsqlitewrapped.h"

#define IOPARSE_SUCCESS 0
#define IOPARSE_FILE_ERR 1
#define IOPARSE_DB_ERR 2

using namespace std;
using namespace boost;
using namespace boost::algorithm;

/* Global Function .. Cannot live without this! */
void help();
int chooseBenchmark (int argc, char *argv[]);
/* - ---------------------------------- */

class IOParsers{
  

 protected:
  int num_runs, csb, aggs, nprocs;
  string bname, input_file_name, output_file_name, dbname;
  string testname;
  Database *db;
  Query *q;
 public:
  IOParsers(int argc, char *argv[]);
  void verify_input();
  int open_file_read (ifstream &ip);
  int open_file_write (ofstream &op);
  list<string> split_tokens (string text, string splits);
  int write_output ();
  string tostring(double);
  string getLine(string search, ifstream &inFile);

  /* SQLITE Database Access Functions */
  int open_database();
  int create_query_object();
  void execute_query(string query);
  bool next_row(Query *q);
  template <class TRET>
    TRET get_val(Query *q, string type);
  /****************************************/

  virtual int io_parser (ifstream &ip);
  
};


#endif
