#include "TileIOParser.h"
#include "BTIOParser.h"


int chooseBenchmark(int argc, char *argv[]){
  
  int  bid = 0, i;
  
  if (argc < 9){
    help();   
  }
  /* Should not damage the original argc,.. We need to resue 
   them inside the object.. This is for have two-level parsing
  of the same arguments*/
  for (i = 0; i < argc; i++){
    if (!strcmp (argv[i], "-b")){
      bid = atoi(argv[i+1]);
      break;
    }
  }
  
  if (bid == 1 ){
    TileIOParser TIOobject(argc, argv);
  }
  if (bid == 2){
    BTIOParser btobject(argc, argv);
  }

  return IOPARSE_SUCCESS;
}
