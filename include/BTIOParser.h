#include "IOParsers.h"

typedef struct {
  double max_wtime;
  double avg_wtime;
  double min_wtime;
  double max_wctime;
  double avg_wctime;
  double min_wctime;
  double max_wexch;
  double avg_wexch;
  double min_wexch;
  double max_rtime;
  double avg_rtime;
  double min_rtime;
  double max_rctime;
  double avg_rctime;
  double min_rctime;
  double max_rexch;
  double avg_rexch;
  double min_rexch;
  double ttime;
  double apptime;
  double io_percent;
  double data_size;
  double bandwidth;
}bt_tuple;

class BTIOParser : public IOParsers{
  
 public:
  BTIOParser(int argc, char *argv[]);
  int io_parser(ifstream &ip);
  void initialize_tuple (bt_tuple &t);
  string construct_query (bt_tuple &t,
			  string tname,
			  string otname);

};


