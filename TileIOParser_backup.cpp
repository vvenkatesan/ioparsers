#include "TileIOParser.h"
/* Sample Output!
# mpi-tile-io run on ubuntu
# 8 process(es) available, 8 used
# filename: tile_128_1k
# collective I/O on
# 0 byte header
# 40 x 160 element dataset, 32 bytes per element
# 4 x 2 tiles, each tile is 10 x 80 elements
# tiles overlap by 0 elements in X, 0 elements in Y
# total file size is 204800 bytes, 1 file(s) total.

# MAX-WRITE AVG-WRITE MIN-WRITE MAX-COMM AVG-COMM MIN-COMM MAX-EXCH AVG-EXCH MIN-EXCH
 0.195672 0.124246 0.001037 0.001821 0.000817 0.000297 0.197556 0.125122 0.001453

# Times are total for all operations of the given type
# Open: min_t = 0.002504, max_t = 0.006056, mean_t = 0.004263, var_t = 0.000003
# Write: min_t = 0.000976, max_t = 0.197731, mean_t = 0.063459, var_t = 0.007799
# Close: min_t = 0.000285, max_t = 0.018459, mean_t = 0.007973, var_t = 0.000069
# Note: bandwidth values based on max_t (worst case)
Write Bandwidth = 0.000 Mbytes/sec 
---- a line between outputs -----------------------------------------------------------


*/


TileIOParser :: TileIOParser(int argc, char *argv[]) :
  IOParsers (argc, argv) {
  ifstream ip;
  open_file_read(ip);
  io_parser(ip);
  
}

int TileIOParser :: io_parser (ifstream &ip){
  
  tile_tuple t;
  char temp_extract[256];
  initialize_tuple(t);
  open_database();
  create_query_object();

  int count = 0, x = 0; 
  string orig_tname = testname;
  /*Lets create the table first */
  string query = "create TABLE if not exists tile_IO (Testname varchar(30), Experiment varchar(30), MaxWtime REAL, AvgWtime REAL, MinWtime REAL, MaxCtime REAL, AvgCtime REAL, MinCtime REAL, MaxExch REAL, AvgExch REAL, MinExch REAL, WBandwidth REAL, PRIMARY KEY(Testname))";
  execute_query(query);
  testname = orig_tname + tostring(x+1);
  /*Lets skip the first 10 lines*/
  while (x < num_runs){

    list<string>vals;
    string splits;
    int list_cnt;
    ip.getline(temp_extract, 256);
    string temp = string(temp_extract);
    trim(temp);
    if (count == -1){
      testname = orig_tname + tostring(x+1);
    }
    if (count > 10){
      switch (count){
      case 11:
	cout << temp << endl;
	splits=" ";
	vals = split_tokens(temp, splits);
	list_cnt = vals.size();
	for (int i=0; i<list_cnt; i++){
	  switch (i){
	  case 0:
	    t.max_wtime = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  case 1:
	    t.avg_wtime = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  case 2:
	    t.min_wtime = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  case 3:
	    t.max_ctime = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  case 4:
	    t.avg_ctime = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  case 5:
	    t.min_ctime = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  case 6:
	    t.max_exch = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  case 7:
	    t.avg_exch = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  case 8:
	    t.min_exch = atof(vals.front().c_str());
	    vals.pop_front();
	    break;
	  }
	}
	break;
      case 18:
	cout << temp <<endl;
	splits = "Write Bandwidth = , Mbytes/sec, ";
	string quote = "\"";
	vals = split_tokens(temp, splits);
	vals.pop_front();
	t.write_bandwidth = atof(vals.front().c_str());
	vals.pop_front();
	vals.erase(vals.begin(), vals.end());
	//This is to skip the additional line..
	//This is where the parsing ends for one entry.. 
	// So lets insert it into the database.

	query = "insert into tile_IO values (" +
	  quote + testname + quote + "," + 
	  quote + orig_tname + quote + "," +
	  tostring(t.max_wtime) + "," +
	  tostring(t.avg_wtime) + "," +
	  tostring(t.min_wtime) + "," +
	  tostring(t.max_ctime) + "," +
	  tostring(t.avg_ctime) + "," +
	  tostring(t.min_ctime) + "," +
	  tostring(t.max_exch) +  "," +
	  tostring(t.avg_exch) + "," +
	  tostring(t.min_exch) + "," +
	  tostring(t.write_bandwidth) + ");";
	execute_query(query);
	
	x++;
	count = -2; 	  
	
	break;
      }
    }
    count++;
  }
  return IOPARSE_SUCCESS;
}


void TileIOParser  ::  initialize_tuple (tile_tuple &t){
  
  t.max_wtime = 0.0;
  t.avg_wtime = 0.0;
  t.min_wtime = 0.0;
  t.max_ctime = 0.0;
  t.avg_ctime = 0.0;
  t.min_ctime = 0.0;
  t.max_exch = 0.0;
  t.avg_exch = 0.0;
  t.min_exch = 0.0;
  t.write_bandwidth = 0.0;

}

