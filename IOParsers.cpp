/*************************************************************************************

IOParser : This application provides an uniform setting for parsing the most widely 
used IO benchmarks and applications. The basic desing also allows ways for easy extension by adding support for newer benchmarks.

This file provides the implementation for the parent class IOParsers.

University of Houston, Copyright 2012.

Vishwanath Venkatesan (venkates@cs.uh.edu)

*****************************************************************************************/



#include "IOParsers.h"


void help(){
  
  cerr <<"\n\n**********************************************"
       <<"\n\nParser for I/O Benchmarks\n"
       << "Vishwanath Venkatesan\n\n"
       <<"**********************************************\n";
  cerr << "i: Input Filename\n"
       << "f: Fcoll Module\n"
       << "m: Mapping Strategy\n" 
       << "n: number of processes\n"
       << "c: cycle buffer size \n"
       << "a: Number of aggregators \n"
       << "b: Name of the benchmark \n"
       << "s: Tile Size, BTIO CLASS \n"
       << "\t 1-->TILEIO\n"
       << "\t 2-->BTIO \n"
       << "r: Number of runs\n"
       << "d: database name (default : io-parser.db)\n"
       << "t: Test Number\n"
       << "h: Help \n\n "
       <<"*************************************************\n\n";
  exit (-1);
}



IOParsers :: IOParsers (int argc,
			char *argv[]){
  
  int c, bid;
  
  if (argc < 8){
    cerr <<"\n\nERROR! Not enough arguments "<< endl;
    help();
  }

  while ((c = getopt (argc, argv, "hi:f:n:c:a:b:r:d:t:m:s:")) != -1){
    switch (c){
    case 'h':
      help();
      break;
    case 'n':
      nprocs = atoi(optarg);
      if (nprocs <= 0){
	cerr << "Number of processes cannot be  <= 0\n";
	exit(-1);
      }
      break;
    case 'c':
      csb = atoi(optarg);
      break;
    case 'a':
      aggs = atoi(optarg);
      break;
    case 'i':
      input_file_name = string(optarg);
      break;
    case 'f':
      fcoll_module = string(optarg);
      break;
    case 't':
      testname = string(optarg);
      break;
    case 'b':
      bid = atoi(optarg);
      switch (bid){
      case 1:
	bname = "TILEIO";
	break;
      case 2:
	bname = "BTIO";
	break;
      case '?':
	cerr <<"Unkown benchmark ID\n";
	abort();
      }
      break;
    case 'r':
      num_runs = atoi(optarg);
      break;
    case 'd':
      dbname  = string(optarg);
      break;
    case 'm':
      mapping = string(optarg);
      break;
    case 's':
      tile_size = string (optarg);
      break;
    case '?':
      if (optopt == 'c')
	fprintf (stderr, "Option -%c requires an argument.\n", optopt);
      else if (isprint (optopt))
	fprintf (stderr, "Unknown option `-%c'.\n", optopt);
      else
	fprintf (stderr,
		 "Unknown option character `\\x%x'.\n",
		 optopt);
      abort();
    default:
      abort ();
    }
  }
}


void IOParsers :: verify_input(){
  cout <<"num_runs : "<<num_runs << endl
       <<"csb : " <<csb << endl
       <<"aggs : " << aggs << endl
       <<"nprocs : " << nprocs << endl
       <<"bname : " << bname << endl
       <<"input file name : " << input_file_name << endl;
}

int IOParsers :: open_file_read (ifstream &ip){

  ip.open(input_file_name.c_str());
  if (!ip.is_open()){
    cerr << "Unable to open the input file: "<< input_file_name <<endl;
    return IOPARSE_FILE_ERR;
  }
  return IOPARSE_SUCCESS;
  
}




list<string> IOParsers ::  split_tokens (string text, string splits){
  list<string> tokenList;
  split(tokenList, text, is_any_of(splits),
	token_compress_on);
  return tokenList;
}

string IOParsers :: tostring (double val){
  ostringstream oss;
  oss << val;
  string s (oss.str());
  return s;
}

int IOParsers ::  open_database(){
  db = new Database(dbname.c_str());
  if (NULL == db){
    return IOPARSE_DB_ERR;
  }
  return IOPARSE_SUCCESS;
}

int IOParsers :: create_query_object(){
  q = new Query(*db);
  if (NULL == q){
    return IOPARSE_DB_ERR;
  }
  return IOPARSE_SUCCESS;
}

void IOParsers :: execute_query (string query){
  q->execute(query.c_str());
}

bool IOParsers :: next_row (Query *q){
  return q->fetch_row();
}

template <class TRET>
TRET IOParsers :: get_val (Query *q, string type){
  switch (type){
  case "string":
    return q->getstr();
    break;
  case "unsigned":
    return q->getuval();
    break;
  case "double":
    return q->getnum();
    break;
  case "bigint":
    return q->getbigint();
    break;
  case "ubigint":
    return q->getubigint();
    break;
  case "?":
    cerr << "Unkown type!\n";
    exit(-1);
    break;
  }
}


string IOParsers :: getLine(string search, ifstream &inFile){
  string line;
  int pos;

  while(!inFile.eof()){
    getline(inFile, line);
    pos = line.find(search);

    if (pos != (int)string::npos){
      return line;
    }
  }
  return NULL;
}


int IOParsers :: io_parser (ifstream &ip){
  return IOPARSE_SUCCESS;
}



