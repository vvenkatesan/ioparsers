CC=g++
SQLITE=/home/vvenkatesan/sqlite3
SQLITEWRAP=/home/vvenkatesan/sqlite_wrapper
#CFLAGS=-g -O0 -Wall -I$(SQLITE)/include/ -I$(SQLITEWRAP)/include/
CFLAGS=-O3 -Wall -I$(SQLITE)/include/ -I$(SQLITEWRAP)/include/
LDFLAGS=-L$(SQLITEWRAP)/lib/ -lsqlitewrapped -L$(SQLITE)/lib/ -lsqlite3 
INCLUDESDIR=include

all :			IOParser

install:
			cp IOParser /usr/bin/

IOParser :		main.o IOParsers.o TileIOParser.o BTIOParser.o benchmarkChooser.o
			$(CC) $(CFLAGS) -o IOParser main.o IOParsers.o TileIOParser.o BTIOParser.o benchmarkChooser.o $(LDFLAGS)

main.o :		main.cpp benchmarkChooser.o
			$(CC) $(CFLAGS) -I$(INCLUDESDIR) -o main.o main.cpp -c 

benchmarkChooser.o :	benchmarkChooser.cpp $(INCLUDESDIR)/TileIOParser.h $(INCLUDESDIR)/BTIOParser.h
			$(CC) $(CFLAGS) -I$(INCLUDESDIR) -o benchmarkChooser.o benchmarkChooser.cpp -c $(LDFLAGS)

IOParsers.o :		IOParsers.cpp $(INCLUDESDIR)/IOParsers.h
			$(CC) $(CFLAGS) -I$(INCLUDESDIR) -o IOParsers.o IOParsers.cpp  -c

TileIOParser.o :	TileIOParser.cpp $(INCLUDESDIR)/TileIOParser.h
			$(CC) $(CFLAGS) -I$(INCLUDESDIR) -o TileIOParser.o TileIOParser.cpp  -c

BTIOParser.o :		BTIOParser.cpp $(INCLUDESDIR)/BTIOParser.h
			$(CC) $(CFLAGS) -I$(INCLUDESDIR) -o BTIOParser.o BTIOParser.cpp  -c

clean:
			rm -rf *.o IOParser *~